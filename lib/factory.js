"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostgresFactory = void 0;
var pg_1 = require("pg");
var query_shoot_1 = require("./query-shoot");
var DEFAULT_CONFIG = { host: "localhost", port: 5432, user: "postgres", database: "postgres", autoClose: true };
/**@constructor*/
var PostgresFactory = /** @class */ (function () {
    function PostgresFactory(configs) {
        if (configs === void 0) { configs = DEFAULT_CONFIG; }
        if (!configs)
            configs = DEFAULT_CONFIG;
        if (configs.autoClose === null || configs.autoClose === undefined)
            configs.autoClose = true;
        configs = Object.assign({}, configs);
        this.configs = configs;
    }
    PostgresFactory.generateUrl = function (config) {
        var empty = Object.assign({}, DEFAULT_CONFIG);
        config = Object.assign(empty, config || {});
        return "postgresql://" + config.user + ":" + config.password + "@" + config.host + ":" + config.port + "/" + config.database;
    };
    /**
     * @param template
     * @param tConfig
     * @return {{queryShoot: QueryShoot, sql: {function, (string[], ...[Translator.values], *): PostgresPromise} }}
     */
    PostgresFactory.prototype.templateOf = function (template, tConfig) {
        if (tConfig === void 0) { tConfig = {}; }
        var client = new pg_1.Client(Object.assign(__assign({}, this.configs), tConfig.connectionConfigs));
        var queryShoot = new query_shoot_1.QueryShoot(client, template, Object.assign({ stream: true }, tConfig.shootConfigs));
        return { sql: queryShoot.sql, queryShoot: queryShoot };
    };
    /**
     * @param connectionConfigs { { ... } }
     * @returns {Pool}
     */
    PostgresFactory.prototype.createPool = function (connectionConfigs) {
        var configs = Object.assign(__assign({}, this.configs), connectionConfigs);
        return new pg_1.Pool(configs);
    };
    PostgresFactory.prototype.createClient = function (connectionConfigs) {
        var configs = Object.assign(__assign({}, this.configs), connectionConfigs);
        return new pg_1.Client(configs);
    };
    /**
     * @param template
     * @return {{queryShoot: QueryShoot, sql: {function, (string[], ...[Translator.values], *): PostgresPromise}}}
     */
    PostgresFactory.prototype.create = function (template) {
        return this.templateOf(template);
    };
    return PostgresFactory;
}());
exports.PostgresFactory = PostgresFactory;
//# sourceMappingURL=factory.js.map