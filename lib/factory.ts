import {Client, ClientConfig, Pool, PoolConfig} from 'pg';
import {QueryShoot, QueryShootConfig} from "./query-shoot";

export type FactoryConfig = ClientConfig&{
    autoClose?: boolean,
    stream?:boolean
};

const DEFAULT_CONFIG:FactoryConfig = { host: "localhost", port: 5432, user: "postgres", database: "postgres", autoClose: true }

/**@constructor*/
export class PostgresFactory {

    private readonly configs: FactoryConfig;

    static generateUrl( config: ClientConfig ) {
        const empty = Object.assign( {}, DEFAULT_CONFIG );
        config = Object.assign( empty, config || {} );
        return `postgresql://${config.user}:${config.password}@${config.host}:${config.port}/${config.database}`;
    }

    constructor( configs:FactoryConfig = DEFAULT_CONFIG ) {
        if (!configs) configs = DEFAULT_CONFIG;
        if ( configs.autoClose === null || configs.autoClose === undefined) configs.autoClose = true;
        configs = Object.assign({}, configs);
        this.configs = configs;
    }

    /**
     * @param template
     * @param tConfig
     * @return {{queryShoot: QueryShoot, sql: {function, (string[], ...[Translator.values], *): PostgresPromise} }}
     */
    templateOf(template, tConfig:{shootConfigs?:QueryShootConfig, connectionConfigs?:ClientConfig } ={}) {

        const client = new Client( Object.assign( { ...this.configs }, tConfig.connectionConfigs) );

        const queryShoot = new QueryShoot(client, template, Object.assign( { stream: true }, tConfig.shootConfigs ) );
        return {sql: queryShoot.sql, queryShoot};
    }

    /**
     * @param connectionConfigs { { ... } }
     * @returns {Pool}
     */
    createPool( connectionConfigs?:PoolConfig ) {
        let configs = Object.assign({...this.configs}, connectionConfigs);
        return new Pool(configs);
    }


    createClient( connectionConfigs?:ClientConfig ) {
        let configs = Object.assign({...this.configs}, connectionConfigs);
        return new Client(configs);
    }


    /**
     * @param template
     * @return {{queryShoot: QueryShoot, sql: {function, (string[], ...[Translator.values], *): PostgresPromise}}}
     */
    create(template) {
        return this.templateOf(template);
    }
}
