"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostgresNotice = void 0;
var result_1 = require("./result");
/**@constructor*/
var PostgresNotice = /** @class */ (function () {
    function PostgresNotice(props) {
        var _this = this;
        this._json = null;
        this._libResult = null;
        Object.keys(props).forEach(function (key) {
            if (!props[key])
                return;
            _this[key] = props[key];
        });
    }
    Object.defineProperty(PostgresNotice.prototype, "json", {
        get: function () {
            if (!this._json)
                this._json = JSON.parse(this.message);
            return this._json;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PostgresNotice.prototype, "asJson", {
        get: function () {
            try {
                return this.json;
            }
            catch (e) {
                return this._json;
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PostgresNotice.prototype, "asResult", {
        /** @return { Result }*/
        get: function () {
            if (this._libResult)
                return this._libResult;
            var json = this.asJson;
            if (!json)
                return null;
            this._libResult = new result_1.Result(json);
            return this._libResult;
        },
        enumerable: false,
        configurable: true
    });
    return PostgresNotice;
}());
exports.PostgresNotice = PostgresNotice;
//# sourceMappingURL=notice.js.map