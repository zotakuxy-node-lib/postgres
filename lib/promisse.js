const { PostgresNotice } =  require( './notice' );

/**
 * @constructor
 * @extends Promise
 */
class PostgresPromise extends Promise{

    #_listeners;

    /**@type QueryShoot*/
    #_queryShoot;
    /**
     * @param executor { function( resolve:function( value:PromiseLike<T> |T ):void, reject:function( reason?:any ):void ):void }
     * @param listeners
     * @param queryShot { QueryShoot }
     */
    constructor( executor, listeners, queryShoot ) {
        super( executor );
        this.#_listeners = listeners;
        this.#_queryShoot = queryShoot;
    }

    /**
     * @param onNotice { function( notice: PostgresNotice ):any  }
     * @return PostgresPromise
     */
    onNotice( onNotice ){
        this.#_listeners.onNotices.push( onNotice );
        return this;
    }

    /**
     * @param onRow { function( row: any ):any  }
     * @return PostgresPromise
     */
    stream ( onRow ){
        this.#_listeners.streams.push( onRow );
        return this;
    }

    /**
     * @param onfulfilled? {  ( value: any ): any, onrejected?: (reason: any ): any  }
     * @param onrejected { function(reason: any ) : PromiseLike<TResult> | TResult }
     * @return PostgresPromise
     */
    then ( onfulfilled, onrejected){
        super.then( onfulfilled );
        return this;
    }

    /**
     * @param onrejected { function(reason: any ) : PromiseLike<TResult> | TResult }
     * @return { PostgresPromise }
     */
    catch ( onrejected ){
        super.catch( onrejected );
        return this;
    }


    /**
     * @return { PostgresPromise }
     * @param onFinally { function() }
     */
    finally( onFinally ){
        super.finally( onFinally );
        return this;
    }

    /**@return {QueryShoot}*/
    get queryShoot(){
        return this.#_queryShoot;
    }


}

module.exports = { PostgresPromise }