"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.QueryShoot = void 0;
var pg_1 = require("pg");
var notice_1 = require("./notice");
var translator_1 = require("./translator");
var PostgresPromise = require("./promisse").PostgresPromise;
var QueryShoot = /** @class */ (function () {
    function QueryShoot(client, template, args) {
        var _this = this;
        if (args === void 0) { args = {}; }
        this._configs = { stream: true, then: false };
        this._notices = [];
        this._onNotice = function (notice, listeners) {
            var pgNotice = new notice_1.PostgresNotice(notice);
            _this.notices.push(pgNotice);
            listeners.onNotices.forEach(function (next) {
                next(pgNotice);
            });
        };
        var _a = (args || {}), stream = _a.stream, then = _a.then;
        var listeners = new Proxy({}, {
            get: function (target, p, receiver) {
                if (target[p] === null || target[p] === undefined)
                    target[p] = [];
                return target[p];
            }
        });
        var iCounter = 0;
        var translator = new translator_1.Translator(function () {
            var promise = new PostgresPromise(function (resolve, reject) {
                var list = _this._configs.then ? [] : null;
                var query = new pg_1.Query(translator.query, translator.values);
                client.connect();
                client.query(query);
                query.on('row', function (row) {
                    if (stream)
                        listeners.streams.forEach(function (listener, index) {
                            listener(row, iCounter);
                        });
                    if (then && list)
                        list.push(row);
                    iCounter++;
                });
                query.on('end', function (args) {
                    var arg = args;
                    Object.defineProperty(arg, "count", {
                        value: iCounter,
                        writable: false,
                        configurable: false
                    });
                    arg.rows = list || [];
                    resolve(arg);
                });
                query.on('error', function (err) {
                    console.error(err);
                    reject(err);
                });
                client.on('notice', function (notice) {
                    _this._onNotice(notice, listeners);
                });
            }, listeners, _this);
            promise.finally(function () {
                try {
                    client.end();
                }
                catch (e) { }
            });
            return promise;
        }, template);
        this.translator = translator;
        this._client = client;
    }
    Object.defineProperty(QueryShoot.prototype, "notices", {
        get: function () { return this._notices; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(QueryShoot.prototype, "hasNotice", {
        get: function () {
            return this.notices.length > 0;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(QueryShoot.prototype, "lastResult", {
        get: function () {
            if (!this.hasNotice || !this.lastNotice)
                return null;
            return this.lastNotice.asResult;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(QueryShoot.prototype, "firstResult", {
        get: function () {
            if (!this.hasNotice || !this.firstNotice)
                return null;
            return this.firstNotice.asResult;
        },
        enumerable: false,
        configurable: true
    });
    QueryShoot.prototype.resultOf = function (index) {
        if (!this.notices[index])
            return null;
        return this.notices[index].asResult;
    };
    QueryShoot.prototype.noticeOf = function (index) {
        return this.notices[index];
    };
    Object.defineProperty(QueryShoot.prototype, "firstNotice", {
        get: function () {
            if (!this.hasNotice)
                return null;
            return this.notices[0];
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(QueryShoot.prototype, "lastNotice", {
        get: function () {
            if (!this.hasNotice)
                return null;
            return this.notices[this.notices.length - 1];
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(QueryShoot.prototype, "connect", {
        get: function () {
            return this;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(QueryShoot.prototype, "sql", {
        get: function () {
            return this.translator.sql;
        },
        enumerable: false,
        configurable: true
    });
    return QueryShoot;
}());
exports.QueryShoot = QueryShoot;
//# sourceMappingURL=query-shoot.js.map