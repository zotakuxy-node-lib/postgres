import {Client, defaults, Query, ResultBuilder} from "pg";
import { PostgresNotice } from "./notice";
import {Result} from "./result";
import { Translator } from "./translator";
const { PostgresPromise } = require( "./promisse" );

export interface PostgresPromise{
    stream( onStream: ( row ) =>any ): PostgresPromise
    onNotice( onNotice: ( notice:PostgresNotice ) =>any ):PostgresPromise
    finally(onfinally?: (() => void) | undefined | null): PostgresPromise
    then< TResult1, TResult2 = never >(onfulfilled?: ((value: TResult1) => TResult1 | PromiseLike<TResult1>), onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) ): PostgresPromise
    catch< TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) ): PostgresPromise,
    queryShoot:QueryShoot
}

export type SQLTranslator = ( strings:TemplateStringsArray, ...values:Array<any> ) => PostgresPromise;

export type QueryShootConfig = { stream?:boolean, then?:boolean };

export class QueryShoot {

    private _client:Client;

    translator:Translator;

    private _configs: QueryShootConfig = { stream: true, then: false };

    private _notices:PostgresNotice[] = [];

    constructor( client:Client, template, args: QueryShootConfig ={} ) {
        const { stream, then } = ( args || {} );

        const listeners: any = new Proxy({}, {
            get(target, p, receiver) {
                if( target[ p ] === null || target[ p ] === undefined ) target[ p ] = [];
                return target[ p ];
            }
        });

        let iCounter = 0;

        const translator = new Translator( ()=>{
            const promise = new PostgresPromise( (resolve, reject ) => {
                const list:any[] | null = this._configs.then ? [] : null;
                const query = new Query(translator.query, translator.values);
                client.connect();
                client.query(query);
                query.on('row', row => {
                    if ( stream )
                        listeners.streams.forEach((listener, index) => {
                            listener(row, iCounter);
                        });
                    if ( then && list ) list.push( row );
                    iCounter++;
                });

                query.on( 'end', (args) => {
                    const arg:ResultBuilder = args;
                    Object.defineProperty( arg, "count", {
                        value: iCounter,
                        writable: false,
                        configurable: false
                    });
                    arg.rows = list || [];
                    resolve( arg );
                })
                query.on( 'error', err => {
                    console.error(err);
                    reject(err);
                });

                client.on('notice', ( notice )=>{
                    this._onNotice( notice, listeners )
                });
            }, listeners, this );

            promise.finally(() => {
                try { client.end(); } catch (e) {}
            });

            return promise;
        }, template );

        this.translator = translator;
        this._client = client;
    }

    private _onNotice = ( notice, listeners )=>{
        const pgNotice = new PostgresNotice( notice );
        this.notices.push(  pgNotice );
        listeners.onNotices.forEach(next => {
            next( pgNotice );
        });
    }

    get notices():PostgresNotice[] { return this._notices; }

    get hasNotice():boolean{
        return this.notices.length >0;
    }

    get lastResult():Result|null{
        if( !this.hasNotice || !this.lastNotice ) return null;
        return this.lastNotice.asResult;
    }

    get firstResult(): Result|null{
        if( !this.hasNotice || !this.firstNotice ) return null;
        return this.firstNotice.asResult;
    }

    resultOf( index ):Result|null{
        if( !this.notices[ index ] ) return null;
        return this.notices[ index ].asResult;
    }

    noticeOf( index ):PostgresNotice{
        return this.notices[ index ];
    }

    get firstNotice():PostgresNotice|null{
        if( !this.hasNotice ) return null;
        return this.notices[0];
    }

    get lastNotice( ) :PostgresNotice| null|undefined{
        if( !this.hasNotice ) return null;
        return this.notices[ this.notices.length-1 ];
    }

    get connect():QueryShoot{
        return this;
    }

    get sql():SQLTranslator{
        return this.translator.sql;
    }
}