"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.catchFirst = exports.catchLast = exports.catchAll = exports.asResult = exports.Result = void 0;
var Result = /** @class */ (function () {
    function Result(_res, err) {
        if (err === void 0) { err = null; }
        var object = typeof _res === "object" && !!_res;
        this.result = _res && object && _res.result;
        this.message = _res && object ? _res.message : null;
        this.data = _res && object ? _res.data : null;
        if (err) {
            this.result = false;
            this.err = err;
            this.message = err.message;
        }
    }
    return Result;
}());
exports.Result = Result;
function asResult(rows, retry) {
    if (retry === void 0) { retry = false; }
    if (!rows || typeof rows !== "object")
        return undefined;
    if (Array.isArray(rows) && !retry) {
        return asResult(rows[0], true);
    }
    var keys = Object.keys(rows);
    if (keys.length === 1 && Object.keys(rows[keys[0]]).length === 3) {
        rows = rows[keys[0]];
        keys = Object.keys(rows);
    }
    if (keys.length === 3
        && keys.includes("result")
        && keys.includes("message")
        && keys.includes("data"))
        return rows;
}
exports.asResult = asResult;
function catchAll(promise) {
    var response = {
        status: true,
        message: null,
        rows: [],
        notices: [],
    };
    return new Promise(function (resolve) {
        promise.onNotice(function (notice) { return response.notices.push(notice); })
            .stream(function (row) { return response.rows.push(row); })
            .then(function (value) { return response.dataInfo = value; })
            .catch(function (reason) {
            response.status = false;
            response.error = reason;
            response.message = reason.message;
        }).finally(function () {
            response.result = promise.queryShoot.lastResult;
            response.lastNotice = promise.queryShoot.lastNotice;
            if (!response.result)
                response.result = asResult(response.rows);
            resolve(response);
        });
    });
}
exports.catchAll = catchAll;
function catchLast(promise) {
    var response = {
        status: true,
        message: null,
    };
    return new Promise(function (resolve, reject) {
        promise.stream(function (nextRow) {
            response.row = nextRow;
        }).onNotice(function (nextNotice) {
            response.notice = nextNotice;
        }).catch(function (reason) {
            response.status = false;
            response.error = reason;
        }).finally(function () {
            response.result = promise.queryShoot.lastResult;
            response.lastNotice = promise.queryShoot.lastNotice;
            if (!response.result)
                response.result = asResult(response.row);
            resolve(response);
        });
    });
}
exports.catchLast = catchLast;
function catchFirst(promise) {
    var response = {
        status: true,
        message: null,
    };
    return new Promise(function (resolve, reject) {
        promise.stream(function (nextRow) {
            if (!response.row)
                response.row = nextRow;
        }).onNotice(function (nextNotice) {
            if (!response.notice)
                response.notice = nextNotice;
        }).catch(function (reason) {
            response.status = false;
            response.error = reason;
        }).finally(function () {
            response.result = promise.queryShoot.firstResult;
            response.lastNotice = promise.queryShoot.firstNotice;
            if (!response.result)
                response.result = asResult(response.row);
            resolve(response);
        });
    });
}
exports.catchFirst = catchFirst;
//# sourceMappingURL=result.js.map