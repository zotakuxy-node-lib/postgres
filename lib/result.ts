import { PostgresPromise, QueryShoot} from "./query-shoot";
import { PostgresNotice} from "./notice";

export class Result {
    readonly result;
    readonly message;
    readonly data;
    readonly err;

    constructor ( _res:any, err:any|null|undefined = null ) {
        const object = typeof _res === "object" && !!_res;
        this.result = _res && object && _res.result;
        this.message = _res && object ? _res.message : null;
        this.data = _res && object? _res.data : null;
        if( err ){
            this.result = false;
            this.err = err;
            this.message = err.message;
        }
    }
}

export type Catch = {
    status: boolean,
    message?: string|null,
    row?: any,
    notice?: PostgresNotice,
    dataInfo?: any,
    error?: any,
    result?:Result|null,
    lastNotice?:PostgresNotice|null
}

export type CatchAll = {
    status: boolean,
    message?: string|null,
    rows: any[],
    notices: PostgresNotice[],
    dataInfo?: any,
    error?: any,
    result?:Result|null,
    lastNotice?:PostgresNotice|null
}

export function asResult( rows:any|any[], retry:boolean=false ){
    if( !rows || typeof rows !== "object" ) return undefined;

    if( Array.isArray( rows ) && !retry ){
        return asResult( rows[0], true )
    }

    let keys = Object.keys( rows );
    if( keys.length === 1 && Object.keys( rows[ keys[0 ] ] ).length === 3 ){
        rows = rows[ keys[ 0 ] ];
        keys = Object.keys( rows );
    }

    if( keys.length === 3
        && keys.includes( "result")
        && keys.includes( "message")
        && keys.includes( "data")
    ) return  rows;

}

export function catchAll( promise: PostgresPromise ): Promise<CatchAll>{
    const response:CatchAll = {
        status: true,
        message: null,
        rows: [],
        notices: [],
    }

    return new Promise( ( resolve )=>{
        promise.onNotice( notice => response.notices.push( notice ) )
            .stream( row => response.rows.push( row ) )
            .then( value => response.dataInfo = value )
            .catch( reason => {
                response.status = false;
                response.error = reason;
                response.message = reason.message;
            }).finally( () => {
                response.result = promise.queryShoot.lastResult;
                response.lastNotice = promise.queryShoot.lastNotice;
                if( !response.result ) response.result = asResult( response.rows );
                resolve( response );
            })
    })
}

export function catchLast ( promise:PostgresPromise ):Promise<Catch>{
    const response:Catch = {
        status: true,
        message: null,
    }

    return new Promise( (resolve, reject) => {
        promise.stream( nextRow => {
            response.row = nextRow;
        }).onNotice( nextNotice => {
            response.notice = nextNotice;
        }).catch( reason => {
            response.status = false;
            response.error = reason;
        }).finally( ( ) => {
            response.result = promise.queryShoot.lastResult;
            response.lastNotice = promise.queryShoot.lastNotice;
            if( !response.result ) response.result = asResult( response.row );
            resolve( response );
        });
    });
}

export function catchFirst ( promise:PostgresPromise ):Promise<Catch>{
    const response:Catch = {
        status: true,
        message: null,
    }

    return new Promise( (resolve, reject) => {
        promise.stream( nextRow => {
            if( !response.row ) response.row = nextRow;
        }).onNotice( nextNotice => {
            if(!response.notice ) response.notice = nextNotice;
        }).catch( reason => {
            response.status = false;
            response.error = reason;
        }).finally( () => {
            response.result = promise.queryShoot.firstResult;
            response.lastNotice = promise.queryShoot.firstNotice;
            if( !response.result ) response.result = asResult( response.row );
            resolve( response );
        });
    });
}