"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Translator = exports.Templates = void 0;
var types_1 = require("./types");
var Templates;
(function (Templates) {
    Templates["LITERAL"] = "LITERAL";
    Templates["PARAMETERIZED"] = "PARAMETERIZED";
})(Templates = exports.Templates || (exports.Templates = {}));
var Translator = /** @class */ (function () {
    function Translator(onTranslated, template) {
        this.query = "";
        this.values = [];
        var self = this;
        this._onTranslated = onTranslated;
        this._template = template;
        var translators = [];
        translators[Templates.PARAMETERIZED] = function (strings) {
            var values = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                values[_i - 1] = arguments[_i];
            }
            var i = 0;
            var args = [];
            var query = strings[i++];
            var counter = 1;
            while (i < strings.length) {
                var param = values[i - 1];
                var original = param;
                if (!(param instanceof types_1.PreparedParameter))
                    param = types_1.Types.__recognize(param);
                if (!(param instanceof types_1.PreparedParameter))
                    throw new Error("unrecognized type of value " + original);
                if (!param.value) {
                    param.name = "null::" + param.type;
                }
                else {
                    param.name = "$" + counter++ + "::" + param.type;
                    args.push(param.value);
                }
                query += "" + param.pre + param.name + param.post;
                query = query + strings[i++];
            }
            self.values = args;
            self.query = query;
            if (onTranslated)
                return onTranslated(self);
        };
        translators[Templates.LITERAL] = function (strings) {
            var values = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                values[_i - 1] = arguments[_i];
            }
            var i = 0;
            var query = strings[i++];
            while (i < strings.length) {
                var param = values[i - 1];
                var original = param;
                if (!(param instanceof types_1.PreparedParameter))
                    param = types_1.Types.__recognize(param);
                if (!(param instanceof types_1.PreparedParameter))
                    throw new Error("unrecognized type of value " + original);
                query += "" + param.pre + param.literal + param.post;
                query = query + strings[i++];
            }
            self.values = [];
            self.query = query;
            if (onTranslated)
                return onTranslated(self);
        };
        this.translator = translators[template];
    }
    Object.defineProperty(Translator.prototype, "sql", {
        get: function () {
            return this.translator;
        },
        enumerable: false,
        configurable: true
    });
    return Translator;
}());
exports.Translator = Translator;
//# sourceMappingURL=translator.js.map