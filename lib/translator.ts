import { Types, PreparedParameter } from "./types";
import {PostgresPromise, SQLTranslator} from "./query-shoot";


export enum Templates {
    LITERAL = "LITERAL",
    PARAMETERIZED = "PARAMETERIZED"
}


export class Translator{
    query:string = "";
    values:any[] = [];

    private _onTranslated:( translator: Translator ) => PostgresPromise;
    private _template;

    private readonly translator: SQLTranslator;
    
    constructor( onTranslated:( translator:Translator ) => PostgresPromise, template: Templates ) {
        const self:Translator = this;
       
        this._onTranslated = onTranslated;
        this._template = template;

        const translators:((strings:string[], ...values:any )=>PostgresPromise)[] = [];
        translators[ Templates.PARAMETERIZED ] = ( strings, ...values )=>{
            let i = 0;
            let args: any[] = [];
            let query =  strings[ i++ ];
            let counter = 1;

            while ( i < strings.length ){
                let param = values[ i - 1 ];
                let original = param;
                if( !( param instanceof PreparedParameter ) )  param = Types.__recognize( param );
                if( !( param instanceof PreparedParameter ) )  throw new Error( `unrecognized type of value ${ original }` );

                if( !param.value ){
                    param.name = `null::${param.type}`;
                } else {
                    param.name = `$${ counter++ }::${ param.type }`;
                    args.push( param.value );
                }
                query += `${ param.pre }${ param.name }${ param.post }`;
                query  =  query + strings[ i++ ];
            }

            self.values = args;
            self.query = query;
            if( onTranslated ) return onTranslated( self );
        };

        translators[ Templates.LITERAL ] = ( strings:string[], ...values:any ) =>{
            let i = 0;
            let query = strings[i++];
            while (i < strings.length) {
                let param = values[ i - 1 ];
                let original = param;
                if( !( param instanceof PreparedParameter ) )  param = Types.__recognize( param );
                if( !( param instanceof PreparedParameter ) )  throw new Error( `unrecognized type of value ${ original }` );
                query += `${ param.pre }${ param.literal }${ param.post }`;
                query = query + strings[i++];
            }
            self.values = [];
            self.query = query;
            if( onTranslated ) return onTranslated( self );
        }

        this.translator = translators[ template ];
    }


    get sql():SQLTranslator{
        return this.translator;
    }
}