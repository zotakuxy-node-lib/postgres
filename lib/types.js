"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Types = exports.Builder = exports.PreparedParameter = void 0;
var PreparedParameter = /** @class */ (function () {
    function PreparedParameter(args) {
        if (args.pre === null || args.pre === undefined)
            args.pre = "";
        if (args.post === null || args.post === undefined)
            args.post = "";
        this.type = args.type;
        this.value = args.value;
        this.literal = args.literal;
        this.simple = args.simple;
        this.pre = args.pre;
        this.post = args.post;
    }
    return PreparedParameter;
}());
exports.PreparedParameter = PreparedParameter;
var literalType = {
    int2: function (value) { return exports.Builder.numericBuilder("int2", value, true); },
    smallint: function (value) { return exports.Builder.numericBuilder("smallint", value, true); },
    int4: function (value) { return exports.Builder.numericBuilder("int4", value, true); },
    int: function (value) { return exports.Builder.numericBuilder("int", value, true); },
    int8: function (value) { return exports.Builder.numericBuilder("int8", value, true); },
    bigint: function (value) { return exports.Builder.numericBuilder("bigint", value, true); },
    numeric: function (value) { return exports.Builder.numericBuilder("numeric", value, false); },
    float: function (value) { return exports.Builder.numericBuilder("float", value); },
    double: function (value) { return exports.Builder.numericBuilder("double", value); },
    boolean: function (value) { return exports.Builder.numericBuilder("boolean", value); },
    varchar: function (value) { return exports.Builder.stringBuilder("varchar", value); },
    text: function (value) { return exports.Builder.stringBuilder("text", value); },
    json: function (value) { return exports.Builder.stringBuilder("json", value); },
    jsonb: function (value) { return exports.Builder.stringBuilder("jsonb", value); },
};
exports.Builder = {
    literal: function (value) {
        if (value === null || value === undefined)
            return "null";
        if (typeof value !== "string")
            value = value.toString();
        return value.split("\\").join("\\\\")
            .split("'").join("\\'")
            .split("$").join("\\$")
            .split('"').join('\\"');
    },
    numericBuilder: function (type, value, int) {
        if (int === void 0) { int = false; }
        var originalValue = value;
        var isNull = exports.Builder.isNullNumber(value);
        if (isNull)
            value = null;
        else
            value = int ? Number(value).toFixed() : Number(value);
        if (isNaN(value))
            throw new Error("invalid number " + originalValue);
        var simple = !isNull ? "" + value : "null";
        var literal = simple + "::" + type;
        return new PreparedParameter({ type: type, value: value, literal: literal, simple: simple });
    },
    isNullText: function (value) {
        return value === undefined || value === null;
    },
    isNullNumber: function (value) {
        return value === undefined || value === null;
    },
    simpleBuilder: function (type, value, literal) {
        return new PreparedParameter({ type: type, value: value, literal: literal, simple: literal });
    },
    dateTimeBuilder: function (type, value) {
        if (!value)
            return this.stringBuilder(type, null);
        else if (value instanceof Date && !isNaN(value.getTime()))
            value = value.toISOString();
        else if (!isNaN(value))
            value = new Date(Number(value)).toISOString();
        var dateTimeTypes = {
            time: function (parts) { return parts.length === 2 ? parts[1] : parts[0]; },
            timetz: function (parts) { return parts.length === 2 ? parts[1] : parts[0]; },
            date: function (parts) { return parts.length === 2 ? parts[1] : parts[0]; },
            timestamp: function (parts) { return parts.join(" "); },
            timestamptz: function (parts) { return parts.join(" "); }
        };
        value = value.toUpperCase();
        var parts = value.split(" ");
        if (parts.length !== 2)
            parts = value.split("T");
        var transform = dateTimeTypes[type](parts);
        return this.stringBuilder(type, transform);
    },
    stringBuilder: function (type, value) {
        if (exports.Builder.isNullText(value))
            value = null;
        else
            value = "" + value.toString();
        var simple = value ? "E'" + exports.Builder.literal(value) + "'" : "null";
        var literal = simple + "::" + type;
        return new PreparedParameter({ type: type, value: value, literal: literal, simple: simple });
    },
    arrayBuilder: function (array, type, variadic) {
        if (!type)
            type = "text";
        if (!literalType[type])
            type = "text";
        var literal = "";
        var simple = "";
        if (array === null || array === undefined) {
            array = "null";
            literal = "null";
        }
        else {
            if (!Array.isArray(array))
                array = [array];
            /** @type {function( value ): PreparedParameter }*/
            var func_1 = literalType[type];
            simple = "";
            array.forEach(function (value) {
                var param = func_1(value);
                if (simple.length > 0)
                    simple += ", ";
                simple += param.simple;
            });
        }
        return new PreparedParameter({
            type: type + "[]",
            value: array,
            simple: simple,
            literal: "array[ " + simple + " ]::" + type + "[]",
            pre: (variadic) ? "variadic " : "",
        });
    },
    booleanBuilder: function (value) {
        if (typeof value === "undefined" || value === null)
            value = null;
        else if (typeof value === "string")
            ["true", "t"].includes(value.toLowerCase());
        else if (typeof value === "number")
            value = !!value;
        else if (typeof value === "bigint")
            value = !!value;
        else
            return exports.Types.boolean(value.toString());
        return exports.Builder.simpleBuilder("boolean", value, value);
    }
};
exports.Types = {
    json: function (value) {
        return exports.Builder.stringBuilder("json", JSON.stringify(value));
    },
    jsonb: function (value) {
        return exports.Builder.stringBuilder("jsonb", JSON.stringify(value));
    },
    text: function (value) {
        return exports.Builder.stringBuilder("text", value);
    },
    varchar: function (value) {
        return exports.Builder.stringBuilder("varchar", value);
    },
    numeric: function (value) {
        return exports.Builder.numericBuilder("numeric", value);
    },
    float: function (value) {
        return exports.Builder.numericBuilder("float", value);
    },
    double: function (value) {
        return exports.Builder.numericBuilder("double precision", value);
    },
    boolean: function (value) {
        return exports.Builder.booleanBuilder(value);
    },
    int2: function (value) {
        return exports.Builder.numericBuilder("int2", value, true);
    },
    smallint: function (value) {
        return exports.Builder.numericBuilder("smallint", value, true);
    },
    int4: function (value) {
        return exports.Builder.numericBuilder("int4", value, true);
    },
    int: function (value) {
        return exports.Builder.numericBuilder("int", value, true);
    },
    integer: function (value) {
        return exports.Builder.numericBuilder("integer", value, true);
    },
    int8: function (value) {
        return exports.Builder.numericBuilder("int8", value, true);
    },
    bigint: function (value) {
        return exports.Builder.numericBuilder("bigint", value, true);
    },
    time: function (time) {
        return exports.Builder.dateTimeBuilder("time", time);
    },
    timetz: function (time) {
        return exports.Builder.dateTimeBuilder("timetz", time);
    },
    date: function (date) {
        return exports.Builder.dateTimeBuilder("date", date);
    },
    timestamp: function (timestamp) {
        return exports.Builder.dateTimeBuilder("timestamp", timestamp);
    },
    timestamptz: function (timestamptz) {
        return exports.Builder.dateTimeBuilder("timestamptz", timestamptz);
    },
    array: function (array, type, variadic) {
        if (!type)
            type = this.__typeOfArray(array);
        return exports.Builder.arrayBuilder(array, type, variadic);
    },
    unknown: function (value) {
        return exports.Builder.stringBuilder("unknown", value);
    },
    __typeOfArray: function (values) {
        if (!values)
            return "text";
        if (!Array.isArray(values))
            throw new Error("Invalid type is not array");
        if (values.length < 1)
            return "text";
        var type = this.__typeOf(values[0]);
        if (type === "unknown")
            return "text";
        return type;
    },
    __typeOfNumeric: function (value) {
        value = Number(value);
        var isInt = Number.isInteger(value);
        if (isInt && value >= -32768 && value <= 32767)
            return "smallint";
        if (isInt && value >= -2147483648 && value <= 2147483647)
            return "integer";
        if (isInt && value >= -9223372036854775808 && value <= 9223372036854775807)
            return "bigint";
        else
            return "numeric";
    },
    __typeOf: function (value) {
        if (value === null || value === undefined)
            return "unknown";
        else if (value instanceof Date)
            return "timestamp";
        else if (typeof value === "number")
            return this.__typeOfNumeric(value);
        else if (typeof value === "bigint")
            return "bigint";
        else if (typeof value === "boolean")
            return "boolean";
        else if (!isNaN(Number(value)) && !Array.isArray(value))
            return this.__typeOfNumeric(value);
        else if (typeof value === "string")
            return "text";
        else if (Array.isArray(value))
            return "array";
        else if (typeof value === "object")
            return "jsonb";
        else
            return "unknown";
    },
    __recognize: function (value) {
        var type = exports.Types.__typeOf(value);
        return exports.Types[type](value);
    }
};
//# sourceMappingURL=types.js.map