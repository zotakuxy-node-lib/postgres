
export type PGBollenTypes   = "boolean";
export type PGNumericTypes  = "smallint"  | "integer"   | "bigint"    | "numeric"   | "double precision"  | "float";
export type PGStringTypes   = "text"      | "varchar";
export type PGDateTypes     = "date"      | "time"      | "timestamp" | "interval";
export type PGObjectTypes   = "jsonb"     | "json";
export type PGSpecialTypes  = "unknown";
export type PGNormalTypes   = PGNumericTypes | PGStringTypes | PGDateTypes | PGObjectTypes | PGBollenTypes;
export type PGTypes         = PGNormalTypes | PGSpecialTypes;


export class PreparedParameter{
  //type, value, literal, simple
  type;
  value;
  literal;
  simple;
  name?;
  pre?;
  post?;

  constructor( args:PreparedParameter  ) {
    if( args.pre === null || args.pre === undefined ) args.pre = "";
    if( args.post === null || args.post === undefined ) args.post = "";
    this.type = args.type;
    this.value = args.value;
    this.literal = args.literal;
    this.simple = args.simple;
    this.pre = args.pre;
    this.post = args.post;
  }
}

const literalType = {
  int2: ( value ) => Builder.numericBuilder( "int2", value, true ),
  smallint: ( value ) => Builder.numericBuilder( "smallint", value, true ),
  int4: ( value ) => Builder.numericBuilder( "int4", value, true ),
  int: ( value ) => Builder.numericBuilder( "int", value, true ),
  int8: ( value ) => Builder.numericBuilder( "int8", value, true ),
  bigint: ( value ) => Builder.numericBuilder( "bigint", value, true ),
  numeric: ( value ) => Builder.numericBuilder( "numeric", value, false ),
  float: ( value ) => Builder.numericBuilder( "float", value ),
  double: ( value ) => Builder.numericBuilder( "double", value ),
  boolean: ( value ) => Builder.numericBuilder( "boolean", value ),
  varchar: ( value ) => Builder.stringBuilder( "varchar", value ),
  text: ( value ) => Builder.stringBuilder( "text", value ),
  json: ( value ) => Builder.stringBuilder( "json", value ),
  jsonb: ( value ) => Builder.stringBuilder( "jsonb", value ),
};

export const Builder = {
  literal  ( value ) {
    if( value === null || value === undefined ) return "null";
    if ( typeof value !== "string" ) value = value.toString();
    return value.split( "\\" ).join( "\\\\" )
        .split( "'" ).join( "\\'" )
        .split( "$" ).join( "\\$" )
        .split( '"').join( '\\"' )
        ;
  },

  numericBuilder ( type, value:any, int:boolean = false ){
    const originalValue = value;
    let isNull = Builder.isNullNumber( value );
    if( isNull ) value = null;

    else value = int? Number( value ).toFixed() : Number( value );
    if( isNaN( value ) ) throw new Error( `invalid number ${ originalValue }` )
    const simple = !isNull? `${ value }`: `null`;
    const literal = `${ simple }::${ type }`;
    return new PreparedParameter( { type, value, literal, simple } ) ;
  },

  isNullText (value) {
    return value === undefined || value === null;
  },

  isNullNumber ( value ) {
    return value === undefined || value === null;
  },

  simpleBuilder( type, value, literal ){
    return new PreparedParameter( { type, value, literal, simple: literal } );
  },

  dateTimeBuilder( type, value ){
    if( !value ) return this.stringBuilder( type, null )
    else if( value instanceof Date && !isNaN( value.getTime() ) ) value = value.toISOString();
    else if( !isNaN( value ) ) value = new Date( Number( value ) ).toISOString();
    const dateTimeTypes = {
      time: ( parts ) => parts.length === 2? parts[ 1 ] : parts[ 0 ],
      timetz: ( parts ) => parts.length === 2? parts[ 1 ] : parts[ 0 ],
      date: ( parts ) => parts.length === 2? parts[ 1 ]: parts[ 0 ],
      timestamp: ( parts ) => parts.join( " " ),
      timestamptz: ( parts ) => parts.join( " " )
    };
    value = value.toUpperCase();
    let parts = value.split( " " );
    if( parts.length !== 2 ) parts = value.split( "T" );
    const  transform = dateTimeTypes[ type ]( parts );
    return this.stringBuilder( type, transform );
  },

  stringBuilder( type, value ){
    if( Builder.isNullText( value ) ) value = null;
    else value = `${ value.toString() }`;
    const simple = value? `E'${ Builder.literal( value ) }'`: `null`;
    const literal = `${ simple }::${ type }`;
    return new PreparedParameter( { type, value, literal, simple } );
  },

  arrayBuilder( array, type, variadic ) {
    if( !type ) type = "text";
    if( !literalType[ type ] ) type = "text";

    let literal = "";
    let simple = "";
    if( array === null || array === undefined ){
      array = "null";
      literal = "null";
    } else {
      if( !Array.isArray( array ) ) array = [ array ];
      /** @type {function( value ): PreparedParameter }*/

      const func = literalType[ type ];
      simple = "";
      array.forEach( value => {
        let param = func( value );
        if( simple.length >0 ) simple += ", ";
        simple += param.simple;
      });
    }

    return new PreparedParameter({
      type: `${ type }[]`,
      value: array,
      simple,
      literal: `array[ ${ simple } ]::${ type }[]`,
      pre : ( variadic )? "variadic ": "",
    })
  },

  booleanBuilder( value ){
    if( typeof value === "undefined" || value === null ) value = null;
    else if( typeof  value === "string" ) [ "true", "t" ].includes( value.toLowerCase() );
    else if( typeof value === "number" ) value = !!value;
    else if( typeof value === "bigint" ) value = !!value;
    else return Types.boolean( value.toString() );
    return Builder.simpleBuilder( "boolean", value, value );
  }
}

export const Types  = {
  json( value ) {
    return Builder.stringBuilder( "json", JSON.stringify( value ) );
  },

  jsonb( value ){
    return Builder.stringBuilder( "jsonb", JSON.stringify( value ) );
  },

  text( value ){
    return Builder.stringBuilder( "text", value );
  },

  varchar( value ){
    return Builder.stringBuilder( "varchar", value );
  },

  numeric( value ){
    return Builder.numericBuilder( "numeric", value );
  },

  float( value ){
    return Builder.numericBuilder( "float", value );
  },

  double( value ){
    return Builder.numericBuilder( "double precision", value );
  },

  boolean( value ){
    return Builder.booleanBuilder( value );
  },


  int2( value ){
    return Builder.numericBuilder( "int2", value, true );
  },

  smallint( value ){
    return Builder.numericBuilder( "smallint", value, true );
  },

  int4( value ){
    return Builder.numericBuilder( "int4", value, true );
  },

  int( value ){
    return Builder.numericBuilder( "int", value, true );
  },

  integer( value ){
    return Builder.numericBuilder( "integer", value, true );
  },

  int8( value ){
    return Builder.numericBuilder( "int8", value, true );
  },

  bigint( value ){
    return Builder.numericBuilder( "bigint", value, true );
  },

  time( time ){
    return Builder.dateTimeBuilder( "time", time );
  },

  timetz( time ){
    return Builder.dateTimeBuilder( "timetz", time );
  },

  date( date ){
    return Builder.dateTimeBuilder( "date", date );
  },

  timestamp( timestamp ){
    return Builder.dateTimeBuilder( "timestamp", timestamp );
  },

  timestamptz( timestamptz ){
    return Builder.dateTimeBuilder( "timestamptz", timestamptz );
  },


  array( array, type?:string, variadic?:boolean ) {
    if( !type ) type = this.__typeOfArray( array );
    return Builder.arrayBuilder( array, type, variadic )
  },

  unknown( value ){
    return Builder.stringBuilder( "unknown", value );
  },

  __typeOfArray( values ){
    if( !values ) return "text";
    if( !Array.isArray( values ) ) throw new Error( "Invalid type is not array" );
    if( values.length < 1 ) return "text";
    let type = this.__typeOf( values[ 0 ] );
    if( type === "unknown" ) return "text";
    return type;
  },

  __typeOfNumeric( value ):PGNumericTypes{
    value = Number( value );
    let isInt = Number.isInteger( value );
    if( isInt && value >= -32768 && value <= 32767 ) return "smallint"
    if( isInt && value >= -2147483648 && value <= 2147483647 ) return "integer"
    if( isInt && value >= -9223372036854775808 && value <= 9223372036854775807 ) return "bigint"
    else return "numeric";
  },

  __typeOf( value ):PGTypes| "array" {
    if( value === null || value === undefined ) return "unknown";
    else if( value instanceof Date )            return "timestamp";
    else if( typeof value === "number" )        return this.__typeOfNumeric( value );
    else if( typeof value === "bigint" )        return "bigint";
    else if( typeof value === "boolean" )       return "boolean";
    else if( !isNaN( Number ( value ) ) && !Array.isArray( value ) )       return this.__typeOfNumeric( value );
    else if( typeof value === "string" )        return  "text";
    else if( Array.isArray( value ) )           return "array"
    else if( typeof value === "object" )        return "jsonb";
    else                                        return "unknown";
  },

  __recognize( value ){
    const type = Types.__typeOf( value );
    return Types[ type ]( value );
  }
}