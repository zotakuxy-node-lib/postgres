"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
require('source-map-support').install();
(function () { return __awaiter(void 0, void 0, void 0, function () {
    var config, _a, PostgresFactory, Templates, Types, ss, _b, sql, queryShoot, user, promise, i;
    return __generator(this, function (_c) {
        config = {
            user: "postgres",
            port: 5432,
            database: "postgres"
        };
        _a = require('../index.js'), PostgresFactory = _a.PostgresFactory, Templates = _a.Templates, Types = _a.Types;
        ss = new PostgresFactory(config);
        _b = ss.templateOf(Templates.LITERAL), sql = _b.sql, queryShoot = _b.queryShoot;
        user = { name: "Daniel" };
        promise = sql(templateObject_1 || (templateObject_1 = __makeTemplateObject([" select * from test() "], [" select * from test() "])));
        console.log({ promise: promise });
        i = 0;
        promise
            .catch(function (reason) { return console.log("catch", i++); })
            .finally(function () { return console.log("finally", i++); })
            .onNotice(function (notice) { return console.log("onNotice", i++); })
            .stream(function (row) { return console.log("stream", i++); })
            .then(function (value) { return console.log("then", i++); });
        console.log("promise is ", { promise: promise });
        return [2 /*return*/];
    });
}); })();
var templateObject_1;
//# sourceMappingURL=index.js.map