"use strict";
function enumerable(values) {
    return function (target, propertyKey, descriptor) {
        if (values.enumerable !== undefined)
            descriptor.enumerable = values.enumerable;
        if (values.configurable !== undefined)
            descriptor.configurable = values.configurable;
    };
}
var Greeter = /** @class */ (function () {
    function Greeter() {
    }
    return Greeter;
}());
//# sourceMappingURL=test-2.js.map