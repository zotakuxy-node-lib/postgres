function enumerable( values:{ enumerable?:boolean, configurable?: boolean}) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        if( values.enumerable !== undefined ) descriptor.enumerable = values.enumerable;
        if( values.configurable !== undefined ) descriptor.configurable = values.configurable;
    };
}

class Greeter {
    // greeting: string;
    // constructor(message: string) {
    //     this.greeting = message;
    // }
    //
    // @enumerable({ enumerable: true})
    // greet() {
    //     return "Hello, " + this.greeting;
    // }
}