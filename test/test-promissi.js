function enumerable(value) {
    return function (target, propertyKey, descriptor ) {
        descriptor.enumerable = value;
    };
}

class Greeter {
    greeting;
    constructor(message) {
        this.greeting = message;
    }

    @enumerable(false)
    greet() {
        return "Hello, " + this.greeting;
    }
}